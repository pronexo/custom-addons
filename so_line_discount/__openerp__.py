# -*- coding: utf-8 -*-
##############################################################################
#
#    This module uses OpenERP, Open Source Management Solution Framework.
#    Copyright (C) 2015-Today BrowseInfo (<http://www.browseinfo.in>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################
{
    "name": "Sale Order Line Discount",
    "version": "1.0",
    "depends": ['sale', 'product_visible_discount'],
    "author": "BrowseInfo",
    "category": "Sale and Discount",
    "description": """
Added new function field that calculate and display the discount in Sale order line.
Discount on Sales order line, Discount on sales, Discount on line
====================================================================================
    """,
    'license':'AGPL-3',
    "website": "www.browseinfo.in",
    "data": [
        'views/sale_discount_view.xml',
    ],
    'qweb': [
    ],
    "auto_install": False,
    "installable": True,
    "application": True,
    "images":['static/description/Banner.png'],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
