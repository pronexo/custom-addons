# -*- coding: utf-8 -*-
#################################################################################
# Author      : Webkul Software Pvt. Ltd. (<https://webkul.com/>)
# Copyright(c): 2015-Present Webkul Software Pvt. Ltd.
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://store.webkul.com/license.html/>
#################################################################################
{
  "name"                 :  "Website Show Password",
  "summary"              :  "Option to View Password at Login and Signup page.",
  "category"             :  "Website",
  "version"              :  "1.0",
  "author"               :  "Webkul Software Pvt. Ltd.",
  "website"              :  "http://www.webkul.com",
  "description"          :  """""",
  "live_test_url"        :  "",
  "depends"              :  ['website_sale'],
  "data"                 :  ['views/auth_signup_login.xml'],
  "images"               :  ['static/description/Banner.png'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
}